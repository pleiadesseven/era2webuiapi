# era2webui

妙なことがあればIssueに

## 動作･開発環境

Python 3.12

    Automatic1111 SDWebUIやsd-script で推奨されているPython verの場合はPromptMalerのchara項目のバックスラッシュ周りの書き換えが必要

AUTOMATIC1111 / stable-diffusion-webui

    Ver 1.7  CSVファイルのプロンプトはVer1.5以降対応版､ 1.7は新しすぎるのでこのVerで特有の問題が出るかは未検証

### 動作の概要

* emueraの関数 `@OUTOPUT_TXT` を使用して `TEXTLOG.ERB` が トリガーされた時点でのeraバリアント内の内部データをJSONの書式で記載されたtxtを出力｡
* `FileHandler`が監視しているフォルダ内のtxtファイルの変更を検知して `SaveJSONHandler`にpathを渡す｡
* SaveJSONHandlerの情報を基に `PromptMaler`が `CSVManager`内のプロンプトを参照してプロンプトを生成｡
* `api`がSDWebUIに生成リクエストを送って画像を生成させる｡
* 動作中の生成パラメーターの変更はconfig.jsonを変更する｡
* 動作中のCSVファイルの変更は `FileHandler`によって監視されているの更新後数秒以内に `CSVManager`へ登録される｡

## 実行方法

1. リポジトリをクローン:

   ```
   git clone https://gitlab.com/pleiadesseven/era2webuiapi.git
   ```
2. 仮想環境を作成:

   ```
   python -m venv venv
   ```
3. 必要なパッケージをインストール:

   ```
   pip install -r requirements.txt
   ```
4. 同梱されているERBフォルダをeraのフォルダに上書き
5. stable-diffusion-webuiのAPIを使用するための引数を追加:

   ```
   --api
   ```
6. era2webuiapiTW.pyを実行:

   ```
   python era2webuiapiTW.py
   ```

### LCM使用による高速化

1. add promptを起動してlcm lora プロンプトを有効化
2. 生成パラメーターをlcm用に設定する

- Sampling method : DPM++ 2S a Karras
- CFG Scale : 2.5~3
- Step 6～8

3. hiresfix使用時は hiresfix step 0

# [StreamDiffusion](https://github.com/cumulo-autumn/StreamDiffusion)対応による処理の超高速化

### 対応環境の変更

StreamDiffusionが対応しているPythonは3.10のため3.12のvenvは削除する

[StreamDiffusion](https://github.com/cumulo-autumn/StreamDiffusion)の説明に従いTorchなどをインストール

config.iniを変更

```config.ini
streamdiffusionで生成する = 1
```

プロンプトを再現する力はイマイチだが早い

東方系統に強いスケベなSDXLモデルとか作らないと駄目かなこれは


# カスタマイズについて

詳しくは別記

# era2webuiTW

`era2webui`は、`era` ゲームのデータをWebUIに変換し、画像を生成するための魔法のツールキットだ。このツールを使えば、キャラクターたちの世界が画面上で魔法のように輝き出す。

## 主要なマジックアイテム

### PromptMaker

このプロンプトの魔術師は、ゲームのシーンやキャラクターの心情を読み取り、それに合わせたプロンプトを生成する。まるでデータから未来を占う占星術師のような存在。

### CSVManager (CSVM)

CSVファイルの宝石箱を開ける鍵。キャラクターや場所、服装、行動に関するエレメントを魔法のように取り出し、プロンプト生成の材料にする。

### SaveJSONHandler (SJH)

セーブデータの守護者で、キャラクターの記憶を管理。このツールがあれば、キャラクターの情報を魔法の光で照らし出し、プロンプト生成の材料にする。

### AI魔理沙に説明書いてもらったけど要領得ない部分があるので補足

eraTW対応に当たってツールのリファクタリングで根幹の機能をごそっと変更しているので過去のversionとの互換性はあまりない
PromptMalerクラス以外は軽微な置換作業でのの対応が可能
開発環境をPython 3.12に変更したため､キャラクタープロンプト部分でバックスラッシュのエスケープ処理も変更されているがこれも単純な書き換え

`CSVManager` クラス プロンプトが定義されたCSVファイルをクラス内部のDictに格納する｡

起動時に1度だけその処理が行われるのでその都度.CSVを読み出していた従来の方式に比べて参照時間が短縮される
CSVに更新があった場合はFileHandlerが検知して､更新を反映させる

`SaveJSONHandler` クラス セーブデータをクラス内Dictに展開する｡そのときStr型で格納された数字はint型に変換｡全角英数字は半角に変換などの処理も行う｡

Queueを受け取るごとにインスタンスを作成してインスタントごとに `PromptMaler`クラスへ渡される｡生成完了後は破棄される｡

`PromptMaler` クラス 前記2つのクラスのDictから要素ごとにプロンプトをクラスDictに格納した後 プロンプト､ネガティブプロンプト､縦横の解像度を出力する

# サンプル画像

ターゲット:霊夢 コマンド:お茶を淹れる
![image](sample_png_00.png)![image](sample_png_01.png)

# CSV

プロンプトCSV 一人じゃ埋めきれない

- [era2webui eraTWプロンプトスプレッドシート](https://docs.google.com/spreadsheets/d/1hxA6WOnmCmW2DNDfd11P0WXDf8l-qfgSCcwViDkK26w/edit?usp=sharing)

# eratohoというコンセプトを模索中

[LoRA For Chinpo](https://civitai.com/models/58319/e2w-v01)

v1は威力弱めなので強度2でもちんこが出たり出なかったり

v2はテストで弄り回してただけなので欠番

v3は威力強めなので強度1でも体がぶっ壊れる

v4はpaperspacebiの強力GPUでLycoris､768x768
なんか知らんがXformarが使えずA6000でもバッチサイズ1でないと動かず5ステップほどしか回してないが過学習気味
0.6ぐらい推奨
階層適用下の値がくどい絵柄にならずにチンコが出る率上がる気がする

```
eratoho_v4:0.6:1:lbw=1,0,0.5,1,0,0,1,0,0,0,0.5,0.5,0.5,0.5,0.5,0.5,1,1,1,0.5,0.5,1,1,0.5,0,0.5
```

## 謝辞

[オリジナルのコード](https://github.com/qulqu3331/era2webuitest)
これを自分で使ってる拡張機能と競合をおこすのでAPIに対応するように書き換えたのがこれ

このプロジェクトを進める上で、多くの人々からの支援を受けた。特に以下の人々には深く感謝する。

- 魔理沙：プロジェクトの進行において、貴重な助言とサポートを提供してくれた。

他にも多くのコミュニティメンバー、貢献者たちに感謝を表したい。みんなの協力がなければ、このプロジェクトはここまで成長しなかったはずだ。
