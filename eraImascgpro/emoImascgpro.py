from module.emo import Expression

class ExpressionImasCgpro(Expression):
    
    def __init__(self, sjh):
        super().__init__(sjh)


    def generate_emotion(self):
        """
        generate_emotionはキャラクターの感情の波を捉え、
        プロンプトとネガティブプロンプトへと変換するメソッドだ。
        このメソッドは、ただエレメントを生成するだけじゃない。
        キャラクターの心の深層に潜むフラグもしっかりと管理して、それぞれの感情表現を細かく調整するんだ。

        トレーニング中のキャラクターには特別な扱いを施し、
        その感情の高まりや痛み、恐怖までもが細かく表現される。
        さらに、目の状態や他キャラとの関係に関するフラグも考慮に入れて、
        エレメントを生成するんだ。

        このメソッドは、各種フラグに応じて、サブメソッドを呼び出し、
        キャラクターの心を豊かに表現するエレメントを組み立てる。

        Returns:
            tuple: (プロンプト文字列, ネガティブプロンプト文字列)
            この二つの文字列は、キャラクターの深層心理を探るための鍵となるエレメントで満ちているぜ。
        """
        self.emotionflags()#
        self.emolevels()   #emolevelの一括設定


        self.create_hp_element() #体力
        self.create_mp_element() #気力


        self.create_embarras_element()#羞恥
        self.create_resist_element() #反発
        self.create_boredom_element() #退屈
        self.create_love_emotion_element() #トキメキ
        self.create_talent_based_element() #顔つき
        #調教に対する反応 TRAIN中のみ反映
        if self.sjh.get_save("scene") == "TRAIN":
            self.create_ahe_element() #絶頂
            self.create_pain_element() #痛み
            self.create_fear_element() #恐怖
            self.create_love_element() #愛情表現 ハートを飛ばす不健全っぽい方

        #目の描写がないならその要素は空文字で抹消
        if not self.flags["ClosedEyes"]:
            self.create_eyes_element()#目色
            self.emopro["目つき"] = ""
            self.emonega["目つき"] = ""
        #主人公が相手でない時は以下2つのプロンプトを抹消
        if not self.flags["主人公以外が相手"]:
            self.emopro["ハート"] = ""
            self.emonega["ハート"] = ""
            self.emopro["反発"] = ""
            self.emonega["反発"] = ""
        self.prompt_debug_emo()
        emopro_values = [value for value in self.emopro.values() if value.strip()]
        emonega_values = [value for value in self.emonega.values() if value.strip()]
        #カンマとスペースを足してヒトツナギに
        prompt = ", ".join(emopro_values)
        negative = ", ".join(emonega_values)
        return prompt,negative