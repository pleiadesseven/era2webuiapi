"""
このモジュールは、eraのテキストログ（TEXTLOG.ERB）が吐き出したJSONデータを、
もっと扱いやすい形に変えるための魔法の工場だぜ！SJHFactoryを使って、
あっという間にSaveJSONHandlerのインスタンスを作るんだ。
そうすれば、JSONデータを手のひらで転がすように扱えるぜ。

キューごとにインスタンスを作って、それを扱うときは引数でインスタンスごと渡すスタイル。
これで、各キューがそれぞれのセーブデータを個別に扱うことができるぜ。
異なる状況や条件に応じた柔軟な処理が可能になるんだ。まるで、異なる魔法の本を同時に読むようなもんだぜ。

例えば、こんな風に使うんだ：
self.save = SJHFactory.create_instance(save_path)

使用例：TaskExecutorで、
prompt, negative, gen_width, gen_height = promptmaker(sjhandler)

Returns:
    SaveJSONHandler: これでeraのセーブデータを自在に操れる。便利だろ？
"""
import sys
import json
import unicodedata

class SJHFactory:
    """
    これは、eraのセーブデータを魔法のように扱う工場だぜ！
    複雑なJSONデータを、あっという間に扱いやすい形に変えるんだ。
    ただのファイルパスを渡すだけで、SaveJSONHandlerがすぐにデータ処理を開始する。
    もう、JSONの複雑さに頭を悩ませることはないぜ！

    Returns:
        SaveJSONHandler: eraのセーブデータをサクサクと処理するためのSaveJSONHandlerインスタンス。
    """
    @staticmethod
    def create_instance(file_path):
        """
        eraのセーブデータを扱うためのツール、SaveJSONHandlerを生成する魔法の呪文だ！
        このメソッドは、SaveJSONHandlerを直接インスタンス化する代わりに、
        ファイルパスからインスタンスを呼び出すために使われる。
        SJHFactory.create_instance("path/to/data.json")で、JSON処理の力を手に入れろ！

        この方法の利点は、インスタンス生成のロジックを一箇所にまとめることで、
        データ処理の方法が一貫していることを保証できる点にある。
        また、将来的な変更や拡張が必要になった場合に、このファクトリークラスを通じて
        容易に対応できるんだ。

        Args:
            file_path (str): eraのセーブデータのファイルパス。TaskExecutorが最新のパスをくれるから楽チンだ。

        Returns:
            SaveJSONHandler: 指定されたファイルパスで動くSaveJSONHandlerインスタンス。
        """
        return SaveJSONHandler(file_path)

class SaveJSONHandler:
    """
    SJHFactoryからtxtのパスを受け取り
    saveのjsonを扱いやすい形にするクラス
    strで保存された数字をintにしたり､忌むべき全角英数字を半角に変換
    boolへの変換はバリアントでkeyやvalueが違うので非対応 奇跡のようなロジックを思いつかない限り
    """
    def __init__(self, savetxt_path=None):
        self.data = None
        if savetxt_path:
            self.load_save(savetxt_path)


    def load_save(self, savetxt_path):
        """渡されたpathからJSONを読んで辞書に格納
        Args:
            savetxt_path (str): txtのpath
        """
        try:
            with open(savetxt_path, 'r', encoding='utf-8_sig') as file:
                self.data = json.load(file)
                self.cast_data()
        except FileNotFoundError as e:
            print(f'エラー: ファイル "{savetxt_path}" が見つかりません。 - {str(e)}')
        except json.JSONDecodeError as e:
            print(
                f'エラー: ファイル "{savetxt_path}" の出力がjsonフォーマットになってない。 - {str(e)}')
            sys.exit()


    def cast_data(self):
        """
        `self.data` のデータ型を必要に応じて変換する。
        """
        # `self.data` を再帰的に辿って型を変換する
        self.data = self._cast_recursive(self.data) #数字だけのstrをint
        self.data = self._convert_to_halfwidth_recursive(self.data) #全角英字を半角英字


    def _cast_recursive(self, data):
        """
        数字が含まれる文字列を整数型に変換したデータを返す再帰的な補助関数。
        キーが文字列型の数字であれば整数型に変換する。
        """
        if isinstance(data, dict):
            new_dict = {}
            for key, value in data.items():
                # キーが文字列型の数字なら整数に変換
                new_key = int(key) if isinstance(key, str) and key.isdigit() else key
                new_dict[new_key] = self._cast_recursive(value)
            return new_dict
        elif isinstance(data, list):
            return [self._cast_recursive(item) for item in data]
        elif isinstance(data, str) and data.isdigit():
            return int(data)  # 文字列が数字のみなら整数に変換
        else:
            return data  # 他の型はそのまま返す


    def _convert_to_halfwidth_recursive(self, data):
        """
        全角英字が含まれる文字列を半角英字に変換したデータを返す再帰的な補助関数。
        文字列が全角英字を含んでいれば、それを半角英字に変換する。
        """
        if isinstance(data, dict):
            new_dict = {}
            for key, value in data.items():
                new_key = self._convert_to_halfwidth_recursive(key)
                new_dict[new_key] = self._convert_to_halfwidth_recursive(value)
            return new_dict
        elif isinstance(data, list):
            return [self._convert_to_halfwidth_recursive(item) for item in data]
        elif isinstance(data, str):
            return unicodedata.normalize('NFKC', data)  # 全角英字を半角英字に変換
        else:
            return data  # 他の型はそのまま返す


    def update_data(self, key, new_value):
        """
        指定されたキーに対して新しい値を設定する。

        この関数は self.data 辞書内の特定のキーを更新する。new_value はどんなデータタイプでも良い。

        Args:
            key (str): 更新するデータのキー。
            new_value: 新しい値（どんなデータタイプでも可）。
        """
        self.data[key] = new_value


    def get_save(self, key):
        """
        お前が渡したキーにピタッと合うデータを、JSONの海から引き上げてくる機能だ。

        JSONファイルはもう読み込んである...といいんだが、もしそれがまだなら
        お前にエラーメッセージを叩きつける。そしたら、適当なファイルを読み込むんだな。
        この関数が呼ばれたら、指定したキーでデータを探してその値を返す。
        もしキーが見当たらなかったらしゃーなし、Noneを返すぞ。

        Args:
            key (str): 辞書型で読み込んだJSONデータから取得したい値のキーだ。

        Returns:
            Any: キーに対応するstr,int,list,dict、もしくはキーが存在しない場合はNone。
        """
        if not self.data:
            #これが出たら意図しない挙動 大変なことよ
            print('エラー: JSONファイルが読み込まれていません。')
            return None
        if key not in self.data:
            #PromptMakerか欲しがっている項目がない場合
            # デバッグ用 好みでコメントアウト
            print(f'エラー: 今は "{key}"って状況にないみたいだぜ')
            return None
        return self.data.get(key)
    