"""
CSVM（CSV Manager）モジュール

このモジュールは、era2webui用に特別に用意されたCSVデータを効率的に管理し、処理するための機能を提供する。
主にCSVファイルの読み込み、データの変換、そしてCSVデータの検索といったタスクを担当するぜ。

機能概要:
- CSVファイルの読み込み: era2webui用に用意されたCSVファイルをPandas DataFrameとして読み込む。
- データ変換: 文字列型の数値を整数型に変換したり、全角数字を半角に変換するなど、データを扱いやすい形式に変換する。
- CSVデータの検索と取得: 特定の条件に基づいてCSVデータから情報を検索し、取得する。

このモジュールの使用により、era2webuiの機能拡張やデータ管理がよりスムーズに、効率的に行えるぜ。
"""
from module.csv_manager import CSVManager


class EraTWCSVManager(CSVManager):
    """
    このクラスは、通常のCSVManagerに一味違う特別な魔法をかけてるんだ！
    「EraTW」専用にカスタマイズされていて、衣装のカテゴリや番号を
    サクサクと追加する能力を持っているぜ。

    通常のCSVManagerが持ってる機能に加えて、
    このEraTWCSVManagerは、「EraTW」の世界で必要とされる特別なデータを扱うんだ。
    衣装のデータをもっと詳細に、もっと便利に扱えるように、
    カテゴリや番号の情報をぱぱっと追加するんだ。

    これで、キャラクターたちの衣装選びがさらに楽しくなること間違いなし！
    衣装の一覧を見たり、特定の衣装を探したりする時に、このクラスが大活躍するぜ。

    Attributes:
        csvlist (str): CSVファイルパスのリストが記載されたCSVファイルへのパス。
        loaded_csvs (dict): 読み込み済みのCSVデータをキャッシュするための辞書。
        # EraTW固有の属性やメソッドがあれば、ここに追加する。

    """
    def __init__(self):
        super().__init__()
        self.add_cloth_columns()
        self.load_display_part()


    def add_cloth_columns(self):
        if 'Cloth.csv' in self.csvdatas:
            df = self.csvdatas["Cloth.csv"]
            columns_to_copy  = {
                'class_name': 'カテゴリ',
                'obj_id': 'カテゴリ内番号',
                'attribute_name': '衣類名',
                'equip_position_no': '装備部位',
                'display_part_no': '表示部位NO',
                'display_part': '表示部位',
                # 他のカラム変換もここに追加
            }
            transformed_df = self.add_columns_with_copy(df, columns_to_copy)
            self.csvdatas["Cloth.csv"] = transformed_df  # 更新されたDataFrameを保存


    def load_display_part(self):
        """
        display_part 辞書に(表示部位:n)のCSVデータを追加
        """
        # Cloth.csv と display_part.csv が存在するかチェック
        if 'Cloth.csv' in self.csvdatas and 'display_part.csv' in self.csvdatas:
            cloth_df = self.csvdatas['Cloth.csv']
            display_part_df = self.csvdatas['display_part.csv']
            # display_partのデータを使ってcloth_dfを更新
            for index, row in cloth_df.iterrows():
                # display_part_dfから対応するdisplay_part_noを検索
                display_part_row = display_part_df[display_part_df['display_part'] == row['表示部位']]
                if not display_part_row.empty:
                    # display_part_noを取得
                    new_display_part_no = display_part_row.iloc[0]['display_part_no']

                    # cloth_dfの両方のカラムに新しい値を設定
                    cloth_df.at[index, 'display_part_no'] = new_display_part_no
                    cloth_df.at[index, '表示部位NO'] = new_display_part_no

            # 更新されたDataFrameをcsvdatasに再代入
            self.csvdatas['Cloth.csv'] = cloth_df
    