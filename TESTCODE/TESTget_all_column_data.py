from module.csv_manager import CSVMFactory

# CSVMインスタンスを生成
csvm = CSVMFactory.get_instance()

# 'Character.csv' から 'キャラ名' 列のデータを取得
all_character_names = csvm.get_all_column_data('Character.csv', 'キャラ名')

print(all_character_names)