import unittest
from eraTW.suberaTW import PromptMaker
from module.savedata_handler import SJHFactory
SAVE_PATH = "H:\\era\\era2webuitest\\eraTW-SD\sav\\txt00.txt"
class TestPromptMaker(unittest.TestCase):
    def setUp(self):
        # テスト用の SJH インスタンスを作成（ダミーデータを使用）
        save = SAVE_PATH
        self.sjh = SJHFactory.create_instance(save)
        # SJH インスタンスを使って PromptMaker インスタンスを作成
        self.prompt_maker = PromptMaker(self.sjh)

    def test_generate_element(self):
        # SJH にダミーデータを設定
        self.sjh.get_save("scene")
        # 状況プロンプトを生成
        prompt,negative,width,height = self.prompt_maker.generate_element()
        # 生成されたプロンプトを確認
        print (prompt)
        print (negative)
        print (width)
        print (height)


if __name__ == '__main__':
    unittest.main()
