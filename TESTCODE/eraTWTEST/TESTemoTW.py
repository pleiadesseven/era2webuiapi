
import unittest
from eraTW.emo import Expression  # Expression クラスが定義されているモジュールをインポート
from module.savedata_handler import SJHFactory  # SJHFactory も必要に応じてインポート

save = f"H:\\era\\era2webuitest\\eraTW-SD\\sav\\txt00.txt"

class TestExpression(unittest.TestCase):
    def setUp(self):
        # テスト用の SJH インスタンスを作成
        self.sjh = SJHFactory.create_instance(save)  # テスト用のダミーデータを使用
        self.expression = Expression(self.sjh)

#pass    # def test_add_emo_element(self):
    #     add_emo_element メソッドのテスト
    #     self.expression.add_element('眠り', 'sleepy eyes', 'bright eyes')
    #     self.assertEqual(self.expression.emopro['眠り'], 'sleepy eyes')
    #     self.assertEqual(self.expression.emonega['眠り'], 'bright eyes')

#Pass 他のメソッドに対するテストケースも同様に追加する
    # def test_generate_emotion(self):
    #     prompt, negative = self.expression.generate_emotion()
    #     # prompt と negative が文字列であることを確認
    #     self.assertIsInstance(prompt, str)
    #     self.assertIsInstance(negative, str)
    #     # 必要であれば、さらに具体的な内容の検証も行う

if __name__ == '__main__':
    unittest.main()
