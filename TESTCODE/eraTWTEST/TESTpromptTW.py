import asyncio
import os
import json
import random
from eraTW.suberaTW import PromptMaker
from module.csv_manager import CSVMFactory
from module.savedata_handler import SJHFactory
from module.api import gen_image_api

SAVE_PATH = os.path.join(os.path.dirname(__file__), "TESTsave.txt")

csvm = CSVMFactory.get_instance()

# ダミーデータの生成
def create_dummy_data():
    dummy_data = {
        "scene": random.choice(["TRAIN", "マスター移動", "ターゲット切替"]),
        "キャラ固有番号":0,
        "target":"",
        "コマンド":0,
        "コマンド名": "",
        "talent": [],
        "上半身着衣状況": random.randint(1, 4),
        "現在位置": "",
        "月": random.choice(["春の月","夏の月","秋の月","冬の月"]),
        "天気": "",
        "日付": random.randint(1, 31),
        "時間": random.randint(1, 1380),
        "success": random.randint(-1, 1),
        "tequip": {},
        "胸の汚れ": random.choice([0, 4]),
        "膣内射精フラグ": 0,
        "射精箇所": random.randint(1, 9),
        "MASTER射精量": random.choice([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]),
        "palam":{"潤滑": random.randint(0, 7500)},
        "処女喪失": random.choice([0, 1]),
        "今回の調教で処女喪失": random.choice([0, 1]),
        "放尿": random.choice([0, 1]),
        "噴乳": random.choice([0, 1]),
        "出産日": random.randint(0, 50)
        # 他の必要なデータも同様に設定
    }

    all_character_names = csvm.get_all_column_data('Character.csv', 'キャラ名')
    random_character = random.choice(all_character_names)
    dummy_data["target"] = random_character
    charaid = csvm.get_df("Character.csv", 'キャラ名', random_character, 'CharaID')
    dummy_data["キャラ固有番号"] = charaid
    all_command_names = csvm.get_all_column_data('Train.csv', 'コマンド名')
    random_command = random.choice(all_command_names)
    dummy_data["コマンド名"] = random_command
    all_talents = csvm.get_all_column_data('Talent.csv', '名称')
    # リストの要素数が10以上の場合にランダムに10個の要素を選ぶ
    random_talent = random.sample(all_talents, 10)
    # 番号をキーにした辞書に変換
    random_talent_dict = {i: talent for i, talent in enumerate(random_talent, start=1)}
    dummy_data["talent"] = random_talent_dict
    all_location = csvm.get_all_column_data('Location.csv', '地名')
    random_location = random.choice(all_location)
    dummy_data["現在位置"] = random_location
    all_weather = csvm.get_all_column_data('Weather.csv', '天気')
    random_weather = random.choice(all_weather)
    dummy_data["天気"] = random_weather
    all_equip = csvm.get_all_column_data('Equip.csv', '名称')
    random_equip = random.sample(all_equip, 3)
    random_equip_dict = {i: talent for i, talent in enumerate(random_equip, start=1)}
    dummy_data["tequip"] = random_equip_dict

    return dummy_data

# ダミーデータをファイルに保存
def save_dummy_data(file_path, data):
    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(data, file, indent=4, ensure_ascii=False)


for i in range(1,50):
    # ダミーデータをTESTsave.txtに保存
    dummy_data = create_dummy_data()
    save_file_path = os.path.join(os.path.dirname(__file__), "TESTsave.txt")
    save_dummy_data(save_file_path, dummy_data)


    save = SJHFactory.create_instance(SAVE_PATH)
    promptmaker = PromptMaker(save)
    prompt,negative,gen_width,gen_height = promptmaker.generate_prompt()
    prompt += "__cameras__ <lora:e2wTW_003-000009:0.7>"
    asyncio.run(gen_image_api(prompt, negative, gen_width, gen_height))