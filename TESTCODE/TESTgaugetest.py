#/sdapi/v1/progressからのprogressを取得して待機ゲージを表示するテスト
import asyncio
import httpx
from tqdm import tqdm

# APIのエンドポイント
url = "http://127.0.0.1:7860"

async def update_progress_bar(client):
    #待機ゲージ初期化
    progress_bar = tqdm(total=100)
    
    #進捗の取得
    while True:
        progress = await client.get(url=f'{url}/sdapi/v1/progress?skip_current_image=false')
        progressjson = progress.json()
        
        if progressjson['progress'] == 0.00: #進捗が0は終了後の値なので0を設定しているが､複数のキューがあるときに処理が終了しない
        
            progress_bar.close()
            break
        progress_bar.update(int((progressjson['progress'] * 100) - progress_bar.n))
        await asyncio.sleep(1)

async def main():
    async with httpx.AsyncClient() as client:
        try:
            await update_progress_bar(client)
        except Exception as e:
            print(f"Error in update_progress_bar: {e}")

asyncio.run(main())